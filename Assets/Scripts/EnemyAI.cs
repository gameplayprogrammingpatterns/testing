﻿/*
- Enemy Logic:
    - Flee: If at very low health run away from enemies
    - Attack: If player is in weapon range attack
    - Fire: If can fire, shoot at player
    - Maintain Range: If can't fire move away from player
    - Close: If player is not in weapon range, move towards player
    - Heal: If player is not in threat range, regenerate some health
*/

public class EnemyAI
{
    public void Update(IEnemy enemy)
    {

    }
}