﻿public class MockEnemy : IEnemy
{
    public bool DidHeal { get; set; }
    public bool DidFire { get; set; }
    public bool DidMoveAway { get; set; }
    public bool DidMoveTowards { get; set; }

    private readonly EnemyAI _ai;

    public MockEnemy()
    {
        _ai = new EnemyAI();
        IsMaxHealth = true;
    }

    public void Update()
    {
        _ai.Update(this);
    }

    public bool Heal()
    {
        DidHeal = true;
        return DidHeal;
    }

    public bool Fire()
    {
        DidFire = CanFire;
        return DidFire;
    }

    public void MoveTowardsTarget()
    {
        DidMoveTowards = true;
    }

    public void MoveAwayFromTarget()
    {
        DidMoveAway = true;
    }

    public bool IsInWeaponRange { get; set; }
    public bool IsInThreatRange { get; set; }
    public bool IsLowHealth { get; set; }
    public bool IsMaxHealth { get; set; }
    public bool CanFire { get; set; }
}