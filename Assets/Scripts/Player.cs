﻿using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _minMovementOffset;

    [SerializeField] private Bullet _bulletPrefab;

    [SerializeField] private Enemy _enemy;

    private void Start ()
    {
        GetComponent<Renderer>().material.color = Color.white;
    }

    private void Update ()
    {
        var pos = gameObject.transform.position;
        var mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mPos.z = 0;
        var offset = mPos - pos;
        if (offset.magnitude > _minMovementOffset)
        {
            var dir = offset.normalized;
            GetComponent<Rigidbody>().AddForce(dir * _speed * Time.deltaTime, ForceMode.Impulse);
        }

        if (Input.GetMouseButtonDown(0))
        {
            var bulletDir = (_enemy.transform.position - transform.position).normalized;
            var bullet = Instantiate(_bulletPrefab);
            bullet.Init(bulletDir, gameObject, _enemy.tag);
        }
    }
}
