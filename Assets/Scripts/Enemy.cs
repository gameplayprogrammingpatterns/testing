﻿using UnityEngine;

public class Enemy : MonoBehaviour, IEnemy
{
    [SerializeField] private Player _player;
    [SerializeField] private float _healRate;
    [SerializeField] private float _speed;
    [SerializeField] private float _fireInterval;
    [SerializeField] private float _weaponRange;
    [SerializeField] private float _threatRange;
    [SerializeField] private float _maxHealth;
    [SerializeField] private float _health;
    [SerializeField] private float _lowHealthThreshold;

    [SerializeField] private Bullet _bulletPrefab;

    private float _gunCoolDown;

    private EnemyAI _ai;

    //////////////////////////////////////////////////////
    // UNITY EVENTS
    //////////////////////////////////////////////////////
    private void Start ()
    {
        GetComponent<Renderer>().material.color = Color.red;
        _health = _maxHealth;
        _ai = new EnemyAI();
    }

    private void Update ()
    {
        _ai.Update(this);

        if (_gunCoolDown > 0)
        {
            _gunCoolDown = Mathf.Max(_gunCoolDown - Time.deltaTime, 0);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            var bullet = other.gameObject.GetComponent<Bullet>();
            if (CompareTag(bullet.TargetTag))
            {
                _health = Mathf.Max(--_health, 0);
            }
        }
    }

    //////////////////////////////////////////////////////
    // HELPER FUNCTIONS
    //////////////////////////////////////////////////////
    private Vector3 DirectionToPlayer()
    {
        return (_player.transform.position - gameObject.transform.position).normalized;
    }

    private float DistanceToPlayer()
    {
        return Vector3.Distance(_player.transform.position, gameObject.transform.position);
    }

    private void Move(Vector3 dir)
    {
        GetComponent<Rigidbody>().AddForce(dir * _speed * Time.deltaTime, ForceMode.Impulse);
    }

    public bool CanFire { get { return _gunCoolDown == 0; } }
    public bool CanHeal { get { return _health < _maxHealth; } }


    //////////////////////////////////////////////////////
    // IENEMY IMPLEMENTATION
    //////////////////////////////////////////////////////
    public bool Heal()
    {
        if (CanHeal)
        {
            _health = Mathf.Min(_health + _healRate * Time.deltaTime, _maxHealth);
            return true;
        }
        return false;
    }

    public void MoveTowardsTarget()
    {
        Move(DirectionToPlayer());
    }

    public void MoveAwayFromTarget()
    {
        Move(-DirectionToPlayer());
    }

    public bool Fire()
    {
        if (CanFire)
        {
            var dir = DirectionToPlayer();
            var bullet = Instantiate(_bulletPrefab);
            bullet.Init(dir, gameObject, _player.tag);
            _gunCoolDown = _fireInterval;
            return true;
        }
        return false;
    }

    public bool IsInWeaponRange { get { return DistanceToPlayer() <= _weaponRange; } }
    public bool IsInThreatRange { get { return DistanceToPlayer() <= _threatRange; } }
    public bool IsLowHealth { get { return _health <= _threatRange; } }
	public bool IsMaxHealth { get { return _health == _maxHealth; } }
}
