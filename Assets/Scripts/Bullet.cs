﻿using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _speed;

    public string TargetTag { get; private set; }

    public void Init(Vector3 direction, GameObject source, string targetTag)
    {
        GetComponent<Renderer>().material.color = source.GetComponent<Renderer>().material.color;
        TargetTag = targetTag;
        var pos = source.transform.position;
        pos += direction * (source.GetComponent<Collider>().bounds.extents.x + Mathf.Epsilon);
        transform.position = pos;
        var body = GetComponent<Rigidbody>();
        body.AddForce(direction * _speed);
        transform.up = direction;
    }

    private void Update ()
    {
        if (!GetComponent<Renderer>().isVisible)
        {
            Destroy(gameObject);
        }
	}

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag(TargetTag))
        {
            Destroy(gameObject);
        }
    }
}
