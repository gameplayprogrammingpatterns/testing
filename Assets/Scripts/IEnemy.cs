﻿/*
This defines a set of methods that need to be supported to have the AI (i.e. if you want to have this AI, this is
the "contract" or interface you have to support)

The reason we're doing this rather than talking directly to an Enemy script is that it allows us to decouple
the AI from the rest of the Enemy implementation. This adds some extra boilerplate but it give the following benefits:
- Forces us to think explicitly about the relationship between the AI and the Enemy script, which pushes us towards a cleaner design
- Isolates the AI from Unity allowing us to test it in isolation.

- Actions: These are how the AI is going to hand off its "work"
    - Heal: Heal the enemy by one tick
    - MoveTowardsTarget: Move towards the player
    - MoveAwayFromTarget: Move away from the player
    - Fire: Fire a bullet at the player
- Queries: These are how the AI gets access to the information it needs to do its work
    - CanFire: Can a bullet be fired or is the gun cooling down?
    - IsInWeaponRange: Is the player close enough to fire at?
    - IsInThreatRange: Is the player too close for comfort?
    - IsLowHealth: Is the enemy at the low health threshold?
    - IsMaxHealth: Is the enemy's health topped off?
*/

using System.CodeDom;

public interface IEnemy
{
    // Actions
    bool Heal(); // Returns true if it was able to heal
    bool Fire(); // Returns true if it was able to fire
    void MoveTowardsTarget();
    void MoveAwayFromTarget();

    // Queries
    bool IsInWeaponRange { get; }
    bool IsInThreatRange { get; }
    bool IsLowHealth { get; }
	bool IsMaxHealth { get; }
    bool CanFire { get; }
}
